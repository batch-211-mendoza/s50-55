// import {Fragment} from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AppNavbar from "./components/AppNavbar.js";
import Home from "./pages/Home.js";
import Courses from "./pages/Courses.js";
import CourseView from "./components/CourseView.js";
import Register from "./pages/Register.js";
import Login from "./pages/Login.js";
import Logout from "./pages/Logout.js";
import Error from "./pages/Error.js";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext.js";

import "./App.css";

function App() {

  // This will be used to store user information and will be used for validating if a user is logged in on the app or not.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on/upon logging out.
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch("http://localhost:4000/users/details", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
    })
  }, []);

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/courses/:courseId" element={<CourseView/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<Error/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
