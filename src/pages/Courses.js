// import coursesData from "../data/courses";
import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses(){
	// console.log(coursesData);

	// Using the map() array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

	// map() returns an array, which we can display in the page via the component function's return statement

	// props are a way to pass any valid JS data from parent component to child component.
	// You can pass as many props as you want. Even functions can be passed as props.

	const [courses, setCourses] = useState();

	useEffect(() => {
		fetch("http://localhost:4000/courses/all")
		.then(response => response.json())
		.then(data => {

			const courseArr = (data.map((course) => {
				return (
					<CourseCard courseProp={course} key={course._id}/>
				)
			}));
			setCourses(courseArr);
		});
	}, [courses]);

	

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}